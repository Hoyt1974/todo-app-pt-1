import React, { Component } from "react";
import todosList from "./todos.json";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import "./App.css";
class App extends Component {
  state = {
    todos: todosList,
    newText: "",
  };

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  addToDo = (e) => {
    if (e.keyCode === 13) {
      const newTodos = this.state.todos;
      const newTodo = {
        userId: 1,
        id: Math.random(),
        title: this.state.newText,
        completed: false,
      };
      newTodos.push(newTodo);
      console.log(newTodos);
      this.setState({ todos: newTodos, newText: "" });
    }
  };

  toggleComplete = (todoId) => (e) => {
    const newTodos = this.state.todos;

    newTodos.forEach((todo) => {
      if (todo.id === todoId) {
        todo.completed = !todo.completed;
      }
    });

    this.setState({ todos: newTodos });
  };

  handleDelete = (todoId) => (e) => {
    const newTodos = this.state.todos.filter((todo) => todo.id);

    this.setState({ todos: newTodos });
  };

  clearCompleted = () => {
    const newTodos = this.state.todos.filter(
      (todo) => todo.completed === false
    );

    this.setState({ todos: newTodos });
  };

  render() {
    return (
      <Router>
        <section className="todoapp">
          <header className="header">
            <h1>todos</h1>
            <input
              className="new-todo"
              name="newText"
              value={this.state.newText}
              onChange={this.handleChange}
              onKeyDown={this.addToDo}
              placeholder="What needs to be done?"
              autofocus
            />
          </header>
          <Route
            exact
            path="/"
            render={() => (
              <TodoList
                todos={this.state.todos}
                toggleComplete={this.toggleComplete}
                handleDelete={this.handleDelete}
              />
            )}
          />
          <Route
            path="/active"
            render={() => (
              <TodoList
                todos={this.state.todos.filter((todo) => !todo.completed)}
                toggleComplete={this.toggleComplete}
                handleDelete={this.handleDelete}
              />
            )}
          />
          <Route
            path="/completed"
            render={() => (
              <TodoList
                todos={this.state.todos.filter((todo) => todo.completed)}
                toggleComplete={this.toggleComplete}
                handleDelete={this.handleDelete}
              />
            )}
          />

          <footer className="footer">
            <span className="todo-count">
              <strong>0</strong> item(s) left
            </span>

            <button className="clear-completed" onClick={this.clearCompleted}>
              Clear completed
            </button>
            <ul>
              <br />
              <br />
              <li>
                <h3>
                  <Link className="link1" to="/">
                    All
                  </Link>
                </h3>
              </li>
              <li>
                <h3>
                  <Link className="link2" to="/active">
                    Active
                  </Link>
                </h3>
              </li>
              <li>
                <h3>
                  <Link className="link3" to="/completed">
                    Completed
                  </Link>
                </h3>
              </li>
            </ul>
          </footer>
        </section>
      </Router>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={this.props.completed}
            onClick={this.props.toggleComplete}
          />
          <label>{this.props.title}</label>
          <button className="destroy" onClick={this.props.toggleComplete} />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              title={todo.title}
              completed={todo.completed}
              toggleComplete={this.props.toggleComplete(todo.id)}
              handleDelete={this.props.handleDelete(todo.id)}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
